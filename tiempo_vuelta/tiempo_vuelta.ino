/*
  https://www.fisicisenzapalestra.com/tecnologia/arduino/allarme-laser-arduino/
  https://www.baldengineer.com/millis-tutorial.html/amp?__twitter_impression=true
*/

#include <SoftwareSerial.h>

static String LIGHT_THRESHOLD_KEY = "LIGHT_THRESHOLD";
static String LIGHT_KEY = "LIGHT";
static String START_VALUE = "1";
static String STOP_VALUE = "0";
static int FREQUENCY = 10000; // time between sensor light messages
int lightThreshold = 200;

SoftwareSerial bluetooth (10, 11); //El pin 10 sera el Rx, y el pin 11 sera el Tx
bool timerStatus = false;
bool statusPrinted = false;
unsigned long current = 0;
unsigned long previous = 0;
int lap = 0;
int sensorLight;
unsigned long lastLightMessageSent = 0;


void setup() {
  Serial.begin(9600);
  bluetooth.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(A0, INPUT);
  Serial.println("SYSTEM STATUS: OFF");
}

void loop() {
  printStatus();
  if (bluetooth.available() > 0) {
    String value = bluetooth.readString();
    Serial.println(value);
    if (value == START_VALUE) {
      onActions();
    } else if (value == STOP_VALUE) {
      offActions();
    } else if (value.startsWith(LIGHT_THRESHOLD_KEY)) {
      value.replace(LIGHT_THRESHOLD_KEY, "");
      lightThreshold = value.toInt();
      Serial.print("Light threshold changed to: ");
      Serial.println(lightThreshold);
    } else {
      Serial.println("Unrecognized value before race start...");
    }
  } else if (!statusPrinted) {
    Serial.println("Waiting to start");
    statusPrinted = true;
  }
  sensorLight = analogRead(A0);
  sendCurrentLight();
  if (timerStatus) {
    bool inLap = false;
    while (sensorLight > lightThreshold && timerStatus) { // Higher when low light
      sensorLight = analogRead(A0);
      if (inLap == false) {
        inLap = true;
        current = millis();
        unsigned long diff = current - previous;
        previous = current;
        if (lap != 0) {
          Serial.print("Time: ");
          Serial.println(diff);
          bluetooth.print(diff);
        } else {
          Serial.println("READY TO RACE");
        }
        lap++;
      }
      checkToStop();
    }
  }
}

void printStatus() {
  if (timerStatus && !statusPrinted) {
    Serial.println("SYSTEM STATUS: ON");
  }
}

void onActions() {
  timerStatus = true;
  bluetooth.print(START_VALUE);
  Serial.println("SYSTEM STATUS: ON");
}

void offActions() {
  timerStatus = false;
  bluetooth.print(STOP_VALUE);
  Serial.println("SYSTEM STATUS: OFF");
}


void checkToStop() {
  String read = bluetooth.readString();
  if (read == STOP_VALUE || read == START_VALUE) { // while we are on race, if start or stop signal is received, we have to stop current race
    offActions();
  }
}

void sendCurrentLight() {
  unsigned long current = millis();
  if (lastLightMessageSent + FREQUENCY < current) {
    String message = String(LIGHT_KEY + String(sensorLight));
    bluetooth.print(message);
    Serial.println(String("LIGHT MESSAGE SENT: " + message));
    lastLightMessageSent = current;
  }
}
