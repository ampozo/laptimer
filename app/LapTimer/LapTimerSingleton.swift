//
//  LapTimerSingleton.swift
//  LapTimer
//
//  Created by Antonio Miguel Pozo Cámara on 22/01/2019.
//  Copyright © 2019 Antonio Miguel Pozo Cámara. All rights reserved.
//

import Foundation
import EVReflection

class LapTimerSingleton {
    
    private init() {}
    static let shared = LapTimerSingleton()
    
    var configuration = Configuration()
    
    func loadHistory() -> History {
        if let json = UserDefaults.standard.string(forKey: Constants.historyKey) {
            return History(json: json)
        }
        return History()
    }
    
    func addToHistory(round: Round) {
        let history = loadHistory()
        history.rounds.append(round)
        let json = history.toJsonString()
        UserDefaults.standard.setValue(json, forKey: Constants.historyKey)
    }
    
    func removeFromHistory(roundIndex: Int) {
        let history = loadHistory()
        history.rounds.remove(at: roundIndex)
        let json = history.toJsonString()
        UserDefaults.standard.setValue(json, forKey: Constants.historyKey)
    }
    
}
