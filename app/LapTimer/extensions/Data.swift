//
//  Data.swift
//  LapTimer
//
//  Created by Antonio Miguel Pozo Cámara on 26/12/2018.
//  Copyright © 2018 Antonio Miguel Pozo Cámara. All rights reserved.
//

import Foundation

extension Data {
    
    init<T>(from value: T) {
        self = Swift.withUnsafeBytes(of: value) { Data($0) }
    }
    
    func to<T>(type: T.Type) -> T {
        return self.withUnsafeBytes { $0.pointee }
    }
}
