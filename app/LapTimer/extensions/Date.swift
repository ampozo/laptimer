//
//  Data.swift
//  LapTimer
//
//  Created by Antonio Miguel Pozo Cámara on 26/12/2018.
//  Copyright © 2018 Antonio Miguel Pozo Cámara. All rights reserved.
//

import Foundation

extension Date {
    
    
    func toString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm dd/MM/yy"
        return formatter.string(from: self)
    }
}
