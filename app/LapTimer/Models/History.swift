//
//  History.swift
//  LapTimer
//
//  Created by Antonio Miguel Pozo Cámara on 22/01/2019.
//  Copyright © 2019 Antonio Miguel Pozo Cámara. All rights reserved.
//

import Foundation
import EVReflection

class History: EVObject {

    var rounds = Array<Round>()

}
