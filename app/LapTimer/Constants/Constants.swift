//
//  Constants.swift
//  LapTimer
//
//  Created by Antonio Miguel Pozo Cámara on 22/01/2019.
//  Copyright © 2019 Antonio Miguel Pozo Cámara. All rights reserved.
//

import Foundation

struct Constants {
    
    static let lapTimerName = "MLT-BT05"
    
    static let lightThreshold = "LIGHT_THRESHOLD"
    static let light = "LIGHT"
    
//    USERDEFAULTS
    static let fastestLapKey = "LAP_TIMER_FL_KEY"
    static let historyKey = "ROUND_HISTORY_KEY"
    
}
