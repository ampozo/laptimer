//
//  SendDataBack.swift
//  LapTimer
//
//  Created by Antonio Miguel Pozo Cámara on 22/01/2019.
//  Copyright © 2019 Antonio Miguel Pozo Cámara. All rights reserved.
//

import Foundation

protocol GetFromVCDelegate{
    func getFromVC(object: NSObject)
}
