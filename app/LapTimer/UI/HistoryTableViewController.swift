//
//  HistoryTableViewController.swift
//  LapTimer
//
//  Created by Antonio Miguel Pozo Cámara on 01/02/2019.
//  Copyright © 2019 Antonio Miguel Pozo Cámara. All rights reserved.
//

import UIKit

class HistoryTableViewController: UITableViewController {

    var rounds = LapTimerSingleton.shared.loadHistory().rounds
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rounds.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell", for: indexPath) as! HistoryTableViewCell
        let round = rounds[indexPath.row]
        cell.driverLabel.text = round.driver
        cell.dateLabel.text = round.date.toString()
        cell.lapsLabel.text?.append(String(round.laps.count))
        cell.fastestLapLabel.text?.append(String(calculateFastestLap(laps: round.laps)))
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 87
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: {_,_ in
            self.rounds.remove(at: indexPath.row)
            self.tableView.reloadData()
            LapTimerSingleton.shared.removeFromHistory(roundIndex: indexPath.row)
        })
        return [deleteAction]
    }

    private func calculateFastestLap(laps: [Double]) -> Double {
        var fastest = Double.greatestFiniteMagnitude
        for lap in laps {
            if lap < fastest {
                fastest = lap
            }
        }
        return fastest
    }

}
