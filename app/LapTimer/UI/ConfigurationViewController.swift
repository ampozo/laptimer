//
//  ConfigurationViewController.swift
//  LapTimer
//
//  Created by Antonio Miguel Pozo Cámara on 22/01/2019.
//  Copyright © 2019 Antonio Miguel Pozo Cámara. All rights reserved.
//

import UIKit

class ConfigurationViewController: UIViewController {
    
    
    @IBOutlet weak var lightThresholdLabel: UILabel!
    @IBOutlet weak var lightSlider: UISlider!
    
    var delegate: GetFromVCDelegate?
    var configuration = Configuration()

    override func viewDidLoad() {
        super.viewDidLoad()
        configuration.lightThreshold = LapTimerSingleton.shared.configuration.lightThreshold
        lightThresholdLabel.text = "Light threshold: " + String(configuration.lightThreshold)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        guard (sender is UISlider) else { return }
        let value = Int((sender as! UISlider).value)
        configuration.lightThreshold = value
        lightThresholdLabel.text = "Light threshold: " + String(value)
    }
    
    @IBAction func doneTouched(_ sender: Any) {
        delegate?.getFromVC(object: configuration)
        self.navigationController?.popViewController(animated: true)
    }
    
}
