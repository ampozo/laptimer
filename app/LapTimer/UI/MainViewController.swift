//
//  ViewController.swift
//  LapTimer
//
//  Created by Antonio Miguel Pozo Cámara on 19/12/2018.
//  Copyright © 2018 Antonio Miguel Pozo Cámara. All rights reserved.
//

import UIKit
import CoreBluetooth
import AVFoundation


class MainViewController: UIViewController, GetFromVCDelegate {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var timeTable: UITableView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var lightStatusImage: UIImageView!
    
    enum TimerStatus: String {
        case on = "1"
        case off = "0"
    }
    
    let lapTimerId = CBUUID(string: "0000ffe0-0000-1000-8000-00805f9b34fb")
    var centralManager: CBCentralManager!
    var lapTimer: CBPeripheral!
    var writableUUID = CBUUID(string: "0000ffe1-0000-1000-8000-00805f9b34fb")
    var writableCharacteristic: CBCharacteristic?
    var laps = Array<Double>()
    var status = TimerStatus.off
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusLabel.text = "Disconnected"
        timeTable.delegate = self
        timeTable.dataSource = self
        actionButton.isEnabled = false
        centralManager = CBCentralManager(delegate: self, queue: nil)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh connection")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.timeTable.addSubview(refreshControl)
    }
    
    @IBAction func startStopTouched(_ sender: Any) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        if let peripheral = lapTimer, let characteristic = writableCharacteristic {
            if status == .off {
                let onData = TimerStatus.on.rawValue.data(using: .utf8)!
                peripheral.writeValue(onData, for: characteristic, type: .withoutResponse)
            } else {
                let offData = TimerStatus.off.rawValue.data(using: .utf8)!
                peripheral.writeValue(offData, for: characteristic, type: .withoutResponse)
                if laps.count > 0 {
                    let alert = UIAlertController(title: "Do you want to save this times?", message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action) in
                        self.resetView()
                    }))
                    alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                        let nameAlert = UIAlertController(title: "Enter racer name", message: nil, preferredStyle: .alert)
                        nameAlert.addTextField { (textField) in
                            textField.placeholder = "Antonio"
                            textField.autocapitalizationType = .words
                        }
                        nameAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            let textField = nameAlert.textFields![0]
                            if textField.text!.isEmpty {
                                self.saveLaps(name: textField.placeholder!)
                            } else {
                                self.saveLaps(name: textField.text!)
                            }
                            self.resetView()
                        }))
                        self.present(nameAlert, animated: true)
                    }))
                    self.present(alert, animated: true)
                }
            }
        } else {
            let alert = UIAlertController(title: "Not connected", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func getLapColor(index: Int) -> UIColor {
        if laps[index] == laps.min() {
            return UIColor(red:0.49, green:0.00, blue:0.56, alpha:1.0)
        } else if index == 0 || laps[index] < laps[index - 1] {
            return UIColor(red:0.20, green:0.49, blue:0.24, alpha:1.0)
        } else {
            return UIColor(red:0.63, green:0.67, blue:0.18, alpha:1.0)
        }
    }
    
    func resetView() {
        self.laps.removeAll()
        self.timeTable.reloadData()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func saveLaps(name: String) {
        let round = Round()
        round.driver = name
        round.laps = laps
        LapTimerSingleton.shared.addToHistory(round: round)
    }
    
    @objc func refresh(_ sender: Any) {
        centralManager.stopScan()
        centralManager.cancelPeripheralConnection(lapTimer)
        self.readyToStart()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            self.centralManager.scanForPeripherals(withServices: nil)
            self.refreshControl.endRefreshing()
            self.laps = Array()
            self.timeTable.reloadData()
        })
    }
    
    func proccessResponse(data:Data) {
        let response = String(data: data, encoding: String.Encoding.utf8)
        guard (response != nil) else {return}
        switch response {
        case TimerStatus.on.rawValue:
            readyToStop()
            break
        case TimerStatus.off.rawValue:
            readyToStart()
            break
        default:
            if var light = response, light.contains(Constants.light) {
                light = light.replacingOccurrences(of: Constants.light, with: "")
                if let value = Int(light) {
                    setStatusView(text: "Connected", visible: true, light: value)
                }
            } else if var timeReceived = Double(response!) {
                timeReceived = timeReceived / 1000
                let actualFastestLap = UserDefaults.standard.double(forKey: Constants.fastestLapKey)
                if actualFastestLap == 0 || actualFastestLap > timeReceived {
                    UserDefaults.standard.set(timeReceived, forKey: Constants.fastestLapKey)
                }
                laps.append(timeReceived)
                timeTable.reloadData()
                timeTable.scrollToRow(at: IndexPath(row: laps.count - 1, section: 1), at: .bottom, animated: true)
                speak(lap: laps.count, time: timeReceived)
            } else {
                print("unknown received data: ", response!)
            }
        }
    }
    
    func readyToStart() {
        status = .off
        actionButton.titleLabel?.text = "START"
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func readyToStop() {
        status = .on
        actionButton.titleLabel?.text = "STOP"
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func speak(lap: Int, time: Double) {
        let timeStr = String(format: "%0.3f", time).replacingOccurrences(of: ".", with: " punto ")
        let sentence = "Vuelta " + String(lap) + ", " + timeStr
        let utterance = AVSpeechUtterance(string: sentence)
        utterance.voice = AVSpeechSynthesisVoice(language: "es-ES")
        
        let synth = AVSpeechSynthesizer()
        synth.speak(utterance)
    }
    
    func getFromVC(object: NSObject) {
        if object is Configuration, lapTimer != nil {
            LapTimerSingleton.shared.configuration.lightThreshold = (object as! Configuration).lightThreshold
            let threshold = Constants.lightThreshold + String((object as! Configuration).lightThreshold)
            let data = threshold.data(using: .utf8)!
            lapTimer.writeValue(data, for: writableCharacteristic!, type: .withoutResponse)
        }
    }
    
    func setStatusView(text: String, visible: Bool, light: Int = 0) {
        lightStatusImage.isHidden = !visible
        if light != 0 {
            if light < LapTimerSingleton.shared.configuration.lightThreshold {
                lightStatusImage.image = UIImage(named: "sun")
            } else {
                lightStatusImage.image = UIImage(named: "moon")
            }
        }
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowConfiguration", segue.destination is ConfigurationViewController {
            (segue.destination as! ConfigurationViewController).delegate = self
        }
    }
}

extension MainViewController: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            setStatusView(text: "Searching for the device", visible: false)
            centralManager.scanForPeripherals(withServices: [lapTimerId])
            break
        case .poweredOff:
            setStatusView(text: "Bluetooth OFF", visible: false)
            break
        case.resetting:
            setStatusView(text: "Resetting bluetooth", visible: false)
            break
        case .unauthorized:
            setStatusView(text: "Bluetooth unauthorized", visible: false)
            break
        case .unknown:
            setStatusView(text: "Bluetooth unknown", visible: false)
            break
        case .unsupported:
            setStatusView(text: "Bluetooth unsupported", visible: false)
            break
        default:
            break
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        lapTimer = peripheral
        lapTimer.delegate = self
        if peripheral.name == Constants.lapTimerName {
            centralManager.stopScan()
            centralManager.connect(lapTimer)
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connected to peripheral ", peripheral.name ?? "no name")
        statusLabel.text = "Connected"
        setStatusView(text: "Connected", visible: true)
        actionButton.isEnabled = true
        lapTimer.discoverServices(nil)
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        statusLabel.text = "Disconnected"
        actionButton.isEnabled = false
        status = .off
        actionButton.titleLabel?.text = "START"
    }
    
}

extension MainViewController: CBPeripheralDelegate {
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services else { return }
        for service in services {
            print("Service discovered: ", service)
            lapTimer.discoverCharacteristics(nil, for: service)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard let characteristics = service.characteristics else { return }
        for characteristic in characteristics {
            peripheral.readValue(for: characteristic)
            if characteristic.uuid == writableUUID {
                writableCharacteristic = characteristic
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        peripheral.setNotifyValue(true, for: characteristic)
        guard let data = characteristic.value else { return }
        proccessResponse(data: data)
    }
}


extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else{
            return laps.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Fastest lap"
        } else{
            return "Laps"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FastestLapTableViewCell", for: indexPath) as! FastestLapTableViewCell
            let fastestLap = UserDefaults.standard.double(forKey: Constants.fastestLapKey)
            if fastestLap > 0 {
                cell.timeLabel.text = String(fastestLap) + "s"
            } else {
                cell.timeLabel.text = "No time registered"
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LapTableViewCell", for: indexPath) as! LapTableViewCell
            cell.numberLabel.text = String(indexPath.row + 1)
            cell.timeLabel?.text = String(laps[indexPath.row]) + "s"
            cell.backgroundColor = getLapColor(index: indexPath.row)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return CGFloat(88)
        }
        return CGFloat(44)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: {_,_ in
            if indexPath.section == 0 {
                UserDefaults.standard.removeObject(forKey: Constants.fastestLapKey)
            } else {
                self.laps.remove(at: indexPath.row)
            }
            self.timeTable.reloadSections([indexPath.section], with: .automatic)
        })
        return [deleteAction]
    }
}


