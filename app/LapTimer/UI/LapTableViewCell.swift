//
//  LapTableViewCell.swift
//  LapTimer
//
//  Created by Antonio Miguel Pozo Cámara on 20/12/2018.
//  Copyright © 2018 Antonio Miguel Pozo Cámara. All rights reserved.
//

import UIKit

class LapTableViewCell: UITableViewCell {

    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
