//
//  LapTimerTests.swift
//  LapTimerTests
//
//  Created by Antonio Miguel Pozo Cámara on 18/02/2019.
//  Copyright © 2019 Antonio Miguel Pozo Cámara. All rights reserved.
//

import XCTest
@testable import LapTimer

class LapTimerTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        let sentence = "Vuelta " + String(5) + ", " + String(format: "%0.3f", 32.147)
        print(sentence)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
